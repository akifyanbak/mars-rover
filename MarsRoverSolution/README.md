# Hepsiburada: Teknik Envanter

Bu proje Hepsiburada iş görüşmesi için gönderilmiş demo projesi için hazırlanmıştır.

# Başlangıç
Aşağıda projenin yapısı, ön koşullar ve kurulum anlatılmaktadır.

## Proje Yapısı 
* [MarsRover.ConsoleApp](./MarsRover.ConsoleApp)
* [MarsRover.Core](./MarsRover.Core)
* [MarsRover.Data](./MarsRover.Data)
* [MarsRover.Service](./MarsRover.Service)
 
* [MarsRover.Service.Tests](./Tests/MarsRover.Service.Tests)

## Kalıtım Bağımlılık Şeması

![Dependency Diagram](https://bitbucket.org/akifyanbak/mars-rover/raw/e17275ffba55bea6f99ab3da5cec2af5552640f9/MarsRoverSolution/Docs/Architecture.png)


## Ön Koşullar
* [.Net Core SDK 3.1](https://dotnet.microsoft.com/download/dotnet-core/3.1)
* [Visual Studio 2019](https://visualstudio.microsoft.com/tr/downloads/)

## Kurulum
1. Projeyi Klonlayınız.
2. Projeyi restore ediniz:
    ```
    dotnet restore
    ```
3. Projeyi build ediniz:
    ```
    dotnet build
    ```
4. Çalıştırın


## Kullanılan Teknolojiler

*   .NET Core 3.1
*   XUnit