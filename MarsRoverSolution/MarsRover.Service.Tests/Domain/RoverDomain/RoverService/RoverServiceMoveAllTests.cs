﻿using MarsRover.Data.Constant;
using MarsRover.Service.Domain.RoverDomain.Model;
using Xunit;

namespace MarsRover.Service.Tests.Domain.RoverDomain.RoverService
{
    public class RoverServiceMoveAllTests : BaseRoverServiceTest
    {
        [Fact]
        public void HappyPath()
        {
            var moveRequest = new MoveModel.Request(5, 5, 1, 2, DirectionEnum.N, "LMLMLMLMM");
            var response = RoverService.MoveAll(moveRequest);

            //1 3 N
            Assert.True(response.EndPositionX == 1 && response.EndPositionY == 3 && response.EndDirection == DirectionEnum.N);
        }

        [Fact]
        public void WrongOutputPath()
        {
            var moveRequest = new MoveModel.Request(5, 5, 1, 2, DirectionEnum.N, "LMLMLMLMM");
            var response = RoverService.MoveAll(moveRequest);

            //1 3 N
            Assert.False(response.EndPositionX == 999 && response.EndPositionY == 999 && response.EndDirection == DirectionEnum.N);
        }
    }
}