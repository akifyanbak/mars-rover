﻿using MarsRover.Service.Domain.RoverDomain;
using MarsRover.Service.Tests.Base;
using Microsoft.Extensions.DependencyInjection;

namespace MarsRover.Service.Tests.Domain.RoverDomain.RoverService
{
    public class BaseRoverServiceTest : BaseTest
    {
        protected readonly IRoverService RoverService;

        public BaseRoverServiceTest()
        {
            RoverService = ServiceProvider.GetService<IRoverService>();
        }
    }
}