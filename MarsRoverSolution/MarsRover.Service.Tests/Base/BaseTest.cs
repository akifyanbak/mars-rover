﻿using MarsRover.Service.Base.Service;
using Microsoft.Extensions.DependencyInjection;

namespace MarsRover.Service.Tests.Base
{
    public class BaseTest
    {
        protected ServiceProvider ServiceProvider;
        public BaseTest()
        {
            var services = new ServiceCollection();
            services.AddMarsRoverServices();
            ServiceProvider = services.BuildServiceProvider();
        }
    }
}