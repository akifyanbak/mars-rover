﻿namespace MarsRover.Data.Constant
{
    public enum DirectionEnum
    {
        /// <summary>
        /// North
        /// </summary>
        N = 1,

        /// <summary>
        /// East
        /// </summary>
        E = 2,

        /// <summary>
        /// South
        /// </summary>
        S = 3,

        /// <summary>
        /// West
        /// </summary>
        W = 4
    }
}