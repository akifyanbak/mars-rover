﻿namespace MarsRover.Data.Context
{
    public class Plateau
    {
        public Plateau(int width, int height)
        {
            MinWidth = 0;
            MinHeight = 0;
            Width = width;
            Height = height;
        }

        public int MinWidth { get; set; }

        public int MinHeight { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }
    }
}