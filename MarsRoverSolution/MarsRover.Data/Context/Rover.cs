﻿using MarsRover.Data.Constant;

namespace MarsRover.Data.Context
{
    public class Rover
    {
        public Rover(Plateau plateau, Position position, DirectionEnum heading)
        {
            Plateau = plateau;
            Position = position;
            Heading = heading;
        }

        public Plateau Plateau { get; set; }

        public Position Position { get; set; }

        public DirectionEnum Heading { get; set; }
    }
}