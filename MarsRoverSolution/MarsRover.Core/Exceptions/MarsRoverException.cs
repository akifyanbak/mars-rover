﻿using System;

namespace MarsRover.Core.Exceptions
{
    public class MarsRoverException : Exception
    {
        public int HttpStatusCode;

        public string HttpErrorMessage;

        protected MarsRoverException(int httpStatusCode)
        {
            HttpStatusCode = httpStatusCode;
        }

        protected MarsRoverException(int httpStatusCode, string httpErrorMessage)
        {
            HttpStatusCode = httpStatusCode;
            HttpErrorMessage = httpErrorMessage;
        }
    }
}