﻿using MarsRover.Data.Constant;
using MarsRover.Service.Base.Service;
using MarsRover.Service.Domain.RoverDomain;
using MarsRover.Service.Domain.RoverDomain.Model;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace MarsRover.ConsoleApp
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var services = new ServiceCollection();
            services.AddMarsRoverServices();
            var serviceProvider = services.BuildServiceProvider();
            var roverService = serviceProvider.GetService<IRoverService>();

            var moveRequest1 = new MoveModel.Request(5, 5, 1, 2, DirectionEnum.N, "LMLMLMLMM");
            var moveRequest2 = new MoveModel.Request(5, 5, 3, 3, DirectionEnum.E, "MMRMMRMRRM");

            var response1 = roverService.MoveAll(moveRequest1);
            var response2 = roverService.MoveAll(moveRequest2);

            Console.WriteLine(response1.EndPositionX + " " + response1.EndPositionY + " " + response1.EndDirection);
            Console.WriteLine(response2.EndPositionX + " " + response2.EndPositionY + " " + response2.EndDirection);
        }
    }
}