﻿using MarsRover.Service.Domain.RoverDomain;
using Microsoft.Extensions.DependencyInjection;

namespace MarsRover.Service.Base.Service
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddMarsRoverServices(this IServiceCollection services)
        {
            services.AddScoped<IRoverService, RoverService>();
            return services;
        }
    }
}