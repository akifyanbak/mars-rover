﻿using MarsRover.Core.Exceptions;

namespace MarsRover.Service.Base.Exceptions
{
    public class MarsRoverServiceException : MarsRoverException
    {
        public MarsRoverServiceException(int httpStatusCode) : base(httpStatusCode)
        {
        }

        public MarsRoverServiceException(int httpStatusCode, string httpErrorMessage) : base(httpStatusCode, httpErrorMessage)
        {
        }
    }
}