﻿using MarsRover.Data.Constant;

namespace MarsRover.Service.Domain.RoverDomain.Model
{
    public class MoveModel
    {
        public class Request
        {
            public Request(int plateauWidth, int plateauHeight, int startPositionX, int startPositionY, DirectionEnum startDirection, string commandLine)
            {
                PlateauWidth = plateauWidth;
                PlateauHeight = plateauHeight;
                StartPositionX = startPositionX;
                StartPositionY = startPositionY;
                StartDirection = startDirection;
                CommandLine = commandLine;
            }

            public int PlateauWidth { get; set; }
            public int PlateauHeight { get; set; }
            public int StartPositionX { get; set; }
            public int StartPositionY { get; set; }
            public DirectionEnum StartDirection { get; set; }
            public string CommandLine { get; set; }
        }

        public class Response
        {
            public int EndPositionX { get; set; }
            public int EndPositionY { get; set; }
            public DirectionEnum EndDirection { get; set; }
        }
    }
}