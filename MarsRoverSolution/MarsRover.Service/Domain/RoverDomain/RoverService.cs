﻿using MarsRover.Data.Constant;
using MarsRover.Data.Context;
using MarsRover.Service.Base.Exceptions;
using MarsRover.Service.Base.Service;
using MarsRover.Service.Domain.RoverDomain.Model;

namespace MarsRover.Service.Domain.RoverDomain
{
    public class RoverService : BaseService, IRoverService
    {
        private void RunCommand(Rover rover, char command)
        {
            if ('L' == command)
                TurnLeft(rover);
            else if ('R' == command)
                TurnRight(rover);
            else if ('M' == command)
            {
                if (!Move(rover))
                {
                    throw new MarsRoverServiceException(452, "Wrong way");
                }
            }
            else
            {
                throw new MarsRoverServiceException(453, "Wrong parameters");
            }
        }

        private bool Move(Rover rover)
        {
            var isMoveAvailable = rover.Plateau.MinWidth <= rover.Position.X
                                  && rover.Position.X <= rover.Plateau.Width
                                  && rover.Plateau.MinHeight <= rover.Position.Y
                                  && rover.Position.Y <= rover.Plateau.Height;
            ;
            if (!isMoveAvailable)
            {
                return false;
            }
            switch (rover.Heading)
            {
                case DirectionEnum.N:
                    rover.Position.Y += 1;
                    break;

                case DirectionEnum.E:
                    rover.Position.X += 1;
                    break;

                case DirectionEnum.S:
                    rover.Position.Y -= 1;
                    break;

                case DirectionEnum.W:
                    rover.Position.X -= 1;
                    break;
            }

            return true;
        }

        private void TurnLeft(Rover rover)
        {
            rover.Heading = ((int)rover.Heading - 1) < (int)DirectionEnum.N ? DirectionEnum.W : (DirectionEnum)((int)rover.Heading - 1);
        }

        private void TurnRight(Rover rover)
        {
            rover.Heading = ((int)rover.Heading + 1) > (int)DirectionEnum.W ? DirectionEnum.N : (DirectionEnum)((int)rover.Heading + 1);
        }

        public MoveModel.Response MoveAll(MoveModel.Request request)
        {
            var rover = new Rover(new Plateau(request.PlateauWidth, request.PlateauHeight),
                                    new Position(request.StartPositionX, request.StartPositionY),
                                    request.StartDirection);

            for (int i = 0; i < request.CommandLine.Length; i++)
            {
                RunCommand(rover, request.CommandLine[i]);
            }
            return new MoveModel.Response
            {
                EndPositionY = rover.Position.Y,
                EndPositionX = rover.Position.X,
                EndDirection = rover.Heading
            };
        }
    }
}