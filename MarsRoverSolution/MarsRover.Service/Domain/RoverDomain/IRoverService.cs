﻿using MarsRover.Service.Base.Service;
using MarsRover.Service.Domain.RoverDomain.Model;

namespace MarsRover.Service.Domain.RoverDomain
{
    public interface IRoverService : IBaseService
    {
        MoveModel.Response MoveAll(MoveModel.Request request);
    }
}